using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraBehaviour : MonoBehaviour {


    public float hSpeed = 1f;
    public float vSpeed = 1f;
    private float xRot = 0f;
    private float yRot = 0f;
    public Camera cam;
    
    // Start is called before the first frame update
    protected void Start() {
        if (!cam) {
            cam = Camera.main;
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    protected void Update() {
        var mX = Input.GetAxis("Mouse X") * hSpeed;
        var mY = Input.GetAxis("Mouse Y") * vSpeed;

        xRot -= mY;
        xRot = Mathf.Clamp(xRot, -90, 90);
        yRot += mX;

        transform.eulerAngles = new Vector3(xRot, yRot, 0);
        
    }
}