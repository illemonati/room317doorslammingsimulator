using System;
using UnityEngine;

public class DoorBehaviour: MonoBehaviour {

    public GameObject doorPivot;
    public AudioSource audioSource;
    public AudioClip rightAudio;
    public AudioClip wrongAudio;
    public float totalRotation = 90;
    public float totalSeconds = 0.5f;
    public bool isOpen = false;
    public float totalTimePassed = 0f;

    protected void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    public void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Player")) {
            if (!isOpen) {
                isOpen = true;
                totalTimePassed = 0.001f;
                if (transform.CompareTag("Room317Door")) {
                    audioSource.PlayOneShot(rightAudio);
                }
                else {
                    audioSource.PlayOneShot(wrongAudio);
                }
                
            }
        }
    }

    public void FixedUpdate() {
        if (totalTimePassed > 0f && totalTimePassed < totalRotation) {
            totalTimePassed += Time.fixedDeltaTime;
            if (totalTimePassed >= totalSeconds) {
                totalTimePassed = 0f;
                return;
            }
            var angleToRotate = Time.fixedDeltaTime / totalSeconds * totalRotation;
            transform.RotateAround(doorPivot.transform.position, Vector3.up, angleToRotate);
        }
    }
}