using UnityEngine;

public class PlayerMovementBehaviour: MonoBehaviour {
    public CharacterController characterController;
    public float gravity = 9.8f;
    public float mSpeedH = 1;
    public float mSpeedV = 1;
    private float gVelocity = 0f;

    protected void Start() {
        characterController = GetComponent<CharacterController>();
    }

    protected void Update() {
        var h = Input.GetAxis("Horizontal") * mSpeedH;
        var v = Input.GetAxis("Vertical") * mSpeedV;
        characterController.Move((transform.right * h + transform.forward * v) * Time.deltaTime);

        if (characterController.isGrounded) {
            gVelocity = 0;
        }
        else {
            gVelocity -= gravity * Time.deltaTime;
            characterController.Move(new Vector3(0, gVelocity, 0));
        }
    }
}
